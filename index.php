<?php 
$thankyou = false;
if ($_POST) {
  if ($_POST['email']) die('spammer');
  $thankyou = true;
  $message = <<<EOM
שם: fname
טלפון: phone
אימייל: liame
אזור מגורים: azor
EOM;
  $subj = "CONTACT request from landing page";

  $to = 'yosefs@9-cp.com';
  $from = 'landing@9-cp.com';
  $headers = "Bcc: michalif@gmail.com,eitan@9-cp.com \r\nReply-to: {$_POST['liame']} \r\nFrom: $from \r\n";
  mail($to, $subj, strtr($message, $_POST), $headers);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="res/style.css">  
  <title>Coleman Outdoor Kitchen</title>
</head>
<body>
<figure>
  <img src="res/top-s.jpg" alt="">
  <img src="res/top.jpg" alt="">
  <figcaption>
    <h2>Coleman Premium <br>Modular Outdoor Kitchen</h2>
    <h2>מטבחי חוץ מודולריים </h2>
    <p>
מטבחי חוץ מודולרים של קולמן מסדרת PREMIUM  משלבים עיצוב עכשווי עם עוצמה של גריל תעשייתי הניתנים להתאמה אישית לכל בית
    </p>
    <a href="https://coleman.com" target=_blank><img src="res/coleman-logo.png"></a>
  </figcaption>
</figure>  
<section class="form">
  <div class="width">
    <h3>לפרטים נוספים אנא מלאו את הפרטים הבאים: </h3>
    <form action="?thankyou" method=post>
      <input name=fname placeholder="שם מלא:*" required type="text">
      <input name=liame placeholder="כתובת אימייל:*" required type="email">
      <input name=phone placeholder="טלפון:*" required type="tel">
      <select required name="azor">
        <option value="">אזור מגורים:*</option>
        <option value="מרכז">מרכז</option>
        <option value="צפון">צפון</option>
        <option value="דרום">דרום</option>
        <option value="ירושלים">ירושלים</option>
      </select>
      <button>שלח</button>
      <input name=email placeholder="כתובת אימייל:*" type="email" style="display: none;" >
    </form>
    <label class="button points" for="pointspop">לנקודות מכירה משווקים מורשים</label>
  </div>
</section>
<section class="main">
  <div class="width">
    <div class="text">
<p>ענקית הגרילים האמריקאית מציגה את סדרת פרימיום,מטבחי חוץ מודולריים.  גרילי הסדרה מצויידים במבער אינפרה – אדום אחורי ושיפוד מסתובב רוטיסרי משטח צליה יצוק פלנצ'ה.  </p>
<p>משטח העבודה רחב הידיים של מטבח החוץ מאפשר סביבת עבודה נוחה לאירוח מושלם.  העיצוב המודרני הופך את הניקוי למהיר ופשוט מתמיד.</p>
<p>חותמת האיכות שלColeman  ניכרת בכל פרט – החל מנירוסטת 304 איכותית וגימורים יוצאי דופן, כגון טריקה שקטה בכל הדלתות ומהמגירות, כירת צד עוצמתית, גלגלים תעשייתיים איכותיים</p>
    </div>
    <div class="gallery">
      <h3>מטבח החוץ ניתן להתאמה אישית בהתאם לגודל החצר, הגג או המרפסת</h3>
      <div dir=ltr>
        <ul>
          <li>
            <img src="res/slide1.jpg" alt="">
            <span>יחידת גריל 4 מבערים עם מבער אחורי וכירת צד</span>
            <sup>דגם 4 מבערים</sup>
          </li>
          <li>
            <img src="res/slide2.jpg" alt="">
            <span>יחידת גריל 6 מבערים עם מבער אחורי וכירת צד</span>
            <sup>דגם 6 מבערים</sup>
          </li>
          <li>
            <img src="res/slide3.jpg" alt="">
            <span>יחידת מטבח עם כיור מנירוסטה עם חיפוי שיש</span>
            <sup>יחידת כיור</sup>
          </li>
          <li>
            <img src="res/slide4.jpg" alt="">
            <span>יחידת מקרר מנירוסטה 304 עם חיפוי שיש</span>
            <sup>יחידת מקרר</sup>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="pros">
  <div class="width">
    <h2>עוד כמה דברים שכדאי לדעת על מטבח החוץ של Coleman</h2>
    <ul>
      <li>המבערים עשויים מברזל יצוק המבטיח צליה אופטימלית</li>
      <li>כל הדגמים מצוידים בכירת צד איכותית</li>
      <li>המטבח מודולרי ומאפשר קיפול של מדפי הצד והרכבת מקרר וכיור ייעודיים</li>
      <li>בכל הדלתות מותקן מנגנון טריקה שקטה</li>
      <li>הגלגלים התעשייתיים ערוכים לנשיאת משקל רב ומבטיחים תנועה חלקה</li>
    </ul>
  </div>
  <input id="pointspop" type="checkbox">
  <div class="popup">
    <label class="close" for="pointspop"></label>
    <h4>משווקים מורשים</h4>
    <dl>
<dt>גרילטאון</dt><dd>סחרוב דוד 5 ראשל"צ<a href="tel:077-230-5027">077-230-5027</a></dd>
<dt>באלקו</dt><dd>עלית הנוער 2 ת"א<a href="tel:077-230-3001">077-230-3001</a></dd>
<dt>ג. חיון</dt><dd>מושב אמונים<a href="tel:077-997-7724">077-997-7724</a></dd>
<dt>ארגמן</dt><dd>המזמרה 2 נס ציונה<a href="tel:077-996-6960">077-996-6960</a></dd>
<dt>בית הפחם</dt><dd>דרך החברון 9/1 באר שבע <a href="tel:077-996-6881">077-996-6881</a></dd>
<dt>כסא בגינה אבן יהודה</dt><dd>צומת הדסים אבן יהודה<a href="tel:077-996-6857">077-996-6857</a></dd>
<dt>כסא בגינה חדרה</dt><dd>רחוב צה"ל 10 מתחם MIX חדרה<a href="tel:077-996-6732">077-996-6732</a></dd>
<dt>שיווק ישיר</dt><dd>האורנים 19 רינתיה<a href="tel:077-996-6695">077-996-6695</a></dd>
<dt>מרכז החימום בסט גז</dt><dd>האומן 22 ירושלים <a href="tel:077-996-6086">077-996-6086</a></dd>
<dt>אלמוג ציוד טכני חיפה</dt><dd>חלוצי התעשייה 31 חיפה <a href="tel:077-996-6071">077-996-6071</a></dd>
<dt>אלמוג ציוד טכני טירת הכרמל</dt><dd>הפטיש 4 טירת הכרמל <a href="tel:077-729-9553">077-729-9553</a></dd>
    </dl>
  </div>
  <nav class=width>
    <a class="button site" href="http://www.colemangrills.co.il/?categoryId=118375" target=_blank>לאתר הגרילים</a>
  </nav>
</section>
<footer>
  <div class="width">
    <a class="nine" href="http://9-cp.com/" target=_blank><img src="res/nine.png" alt=""></a>
    <div class="copy">Coleman Outdoor Kitchen  קולמן מטבחי חוץ</div>
  </div>
</footer>  
<?php if (isset($_GET['thankyou'])): ?>
<div class="thankyou">
  <label class="close" for=""></label>
  <h3>תודה, הודעתך נשלחה</h3>
</div>
<?php endif; ?>
<script src="//code.jquery.com/jquery-2.1.4.js"></script>
<script src="//stephband.info/jquery.event.move/js/jquery.event.move.js"></script>
<script src="//stephband.info/jquery.event.swipe/js/jquery.event.swipe.js"></script>
<script src="res/unslider-min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
  $('.gallery div').unslider()
  $('.thankyou .close').click(function() { $(this).parent().hide() });
}, false);
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '280227382401600'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=280227382401600&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 857534266;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/857534266/?guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>
